# lege regel nodig?
#!/usr/bin/env python

from string import ascii_lowercase

r1 = [12, 24, 1, 25, 3, 2, 22, 19, 8, 7, 21, 14, 11, 23, 9, 6, 10, 20, 5, 13, 17, 0, 15, 16, 18, 4]
#r1 = [4, 10, 12, 5, 11, 6, 3, 16, 21, 25, 13, 19, 14, 22, 24, 7, 23, 20, 18, 15, 0, 8, 1, 17, 2, 9]
r2 = [0, 9, 3, 10, 18, 8, 17, 20, 23, 1, 11, 7, 22, 19, 12, 2, 16, 6, 25, 13, 15, 24, 5, 21, 14, 4]
r3 = [1, 3, 5, 7, 9, 11, 2, 15, 17, 19, 23, 21, 25, 13, 24, 4, 8, 22, 6, 0, 10, 12, 20, 18, 16, 14]

def rotate_forward(ascii_decimal):
    alphabet_index = ascii_decimal-97

    shift = r1[alphabet_index]
    if shift + alphabet_index > 25: shift -= 26

    return ascii_decimal + shift

def rotate_backward(ascii_decimal):
    alphabet_index = ascii_decimal-97
    shift=0
    number_of_matches_found=0

    for i, j in enumerate(r1):
        val = i+j if i+j<26 else i+j-26
        if val == alphabet_index:
            shift = j
            number_of_matches_found+=1

    if number_of_matches_found==0: print('Geen match gevonden!')
    if number_of_matches_found>1: print('Meer dan 1 match gevonden, namelijk:', number_of_matches_found)

    if (ascii_decimal - shift > 122): return ascii_decimal - shift - 26
    if (ascii_decimal - shift < 97): return ascii_decimal - shift + 26
    return ascii_decimal - shift

def encrypt(ascii_decimals):

    for i, dec in enumerate(ascii_decimals):
        print('INPUT: ', ascii_decimals[i]-97, chr(ascii_decimals[i]))
        ascii_decimals[i] = rotate_forward(ascii_decimals[i])
        print('FORWARD RESULT: ', ascii_decimals[i]-97, chr(ascii_decimals[i]))
        ascii_decimals[i] = rotate_backward(ascii_decimals[i])
        print('BACKWARD RESULT: ', ascii_decimals[i]-97, chr(ascii_decimals[i]))

    return ascii_decimals

def main():
    for i, j in enumerate(r1):
        prefix0 = ' ' if i<10 else ''
        prefix1 = ' ' if i<10 else ''
        prefix2 = ' ' if j<10 else ''
        encrypted_index = i+j+97
        if encrypted_index>122: encrypted_index-=26
        print (chr(i+97),prefix0+str(ord(chr(i))),' => r1['+prefix1+str(i)+'] = ',prefix2+str(j),' => ', chr(encrypted_index),''+str(encrypted_index-97)+'')

    while True:
        msg = input('Enigma >>> ')
        msg = msg.lower()
        if msg == '' or False in [x in ascii_lowercase + ' ' for x in msg]: break
        msg_numbers = [(ord(i)) for i in msg if i != ' ']
        print(''.join([chr(number) for number in encrypt(msg_numbers)]))

if __name__ == '__main__':
    main()
